<?php defined('SYSPATH') OR die('No direct access allowed.');

class Kohana_Obj {

	/**
	 * Retrieve a single property from an object. If the key does not exist in
	 * the object, the default value will be returned instead.
	 *
	 *     // Get the value "username" from $object, if it exists
	 *     $username = obj::get($object, 'username');
	 *
	 * @param   object  object to extract from
	 * @param   string  property name
	 * @param   mixed   default value
	 * @return  mixed
	 */
	public static function get($object, $key, $default = NULL)
	{
		return (is_object($object) AND isset($object->{$key})) ? $object->{$key} : $default;
	}

	public static function get_var( & $var, $default = NULL)
	{
		return isset($var) ? $var : $default;
	}

	public static function parse_boolean($obj)
	{
		return filter_var($obj, FILTER_VALIDATE_BOOLEAN);
	}

	public static function to_array($object)
	{
		if ( ! is_object($object) AND ! is_array($object))
			return $object;

		$array = array();

		foreach ($object as $key => $value)
		{
			$array[$key] = obj::to_array($value);
		}

		return $array;
	}

} // End Kohana_Obj